package com.dextra.meuspedidos.meuspedidosdextra.Cardapio;

import com.dextra.meuspedidos.meuspedidosdextra.Lanche.Lanche;

import java.util.ArrayList;

/**
 * Created by Felipe on 12/1/18.
 */

public final class Cardapio {
    private static final Cardapio INSTANCE = new Cardapio();

    ArrayList<Lanche> cardapio;

    private Cardapio(){
        cardapio = new ArrayList<Lanche>();
    }

    public static Cardapio getInstance(){
        return INSTANCE;
    }

    public void addLanche(Lanche lanche){
        cardapio.add(lanche);
    }

    public Lanche getLanche(int id){
        return cardapio.get(id);
    }

    public ArrayList<Lanche> getCardapio(){
        return cardapio;
    }

    public int getSize(){
       return cardapio.size();
    }

}
