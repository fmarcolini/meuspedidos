package com.dextra.meuspedidos.meuspedidosdextra.Ingredientes;

/**
 * Created by Felipe on 12/2/18.
 */

public class Ingrediente {

    public Number mId;
    public String mName;
    public float mPrice;
    public String mImageURL;

    public Ingrediente(){

    }
}
