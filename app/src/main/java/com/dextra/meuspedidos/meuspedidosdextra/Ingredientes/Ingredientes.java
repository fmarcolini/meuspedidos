package com.dextra.meuspedidos.meuspedidosdextra.Ingredientes;

import com.dextra.meuspedidos.meuspedidosdextra.Promocoes.Promocao;

import java.util.ArrayList;

/**
 * Created by Felipe on 12/2/18.
 */

public class Ingredientes {

    private static final Ingredientes ourInstance = new Ingredientes();
    private ArrayList<Ingrediente> mIngrediente;

    public static Ingredientes getInstance() {
        return ourInstance;
    }

    private Ingredientes(){
        mIngrediente = new ArrayList<Ingrediente>();
    }

    public void addIngrediente(Ingrediente ingrediente){
        mIngrediente.add(ingrediente);
    }

    public String getIngrediente(int id){
        return mIngrediente.get(id).mName;
    }
    public float getIngredientePreco(int id){
        return mIngrediente.get(id).mPrice;
    }

    public ArrayList<Ingrediente> getIngredientes(){
        return mIngrediente;
    }

}
