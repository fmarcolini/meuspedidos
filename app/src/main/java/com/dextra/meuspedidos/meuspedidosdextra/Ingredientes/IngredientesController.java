package com.dextra.meuspedidos.meuspedidosdextra.Ingredientes;

import android.content.Context;

/**
 * Created by Felipe on 12/2/18.
 */

public class IngredientesController {

    private IngredientesModel mIngredientesModel;

    public IngredientesController(Context context){
        mIngredientesModel = new IngredientesModel(context);
    }

    public void getIngredientes(){
        mIngredientesModel.loadIngredientes();
    }


}
