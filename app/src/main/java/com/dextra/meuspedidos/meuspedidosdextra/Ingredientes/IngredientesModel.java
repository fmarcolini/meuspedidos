package com.dextra.meuspedidos.meuspedidosdextra.Ingredientes;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dextra.meuspedidos.meuspedidosdextra.Promocoes.Promocao;
import com.dextra.meuspedidos.meuspedidosdextra.Promocoes.Promocoes;
import com.dextra.meuspedidos.meuspedidosdextra.Promocoes.PromocoesModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Felipe on 12/2/18.
 */

public class IngredientesModel {

    private Context mContext;
    private Gson gson;

    public IngredientesModel(Context context){
        mContext = context;
        gson = new GsonBuilder().registerTypeAdapter(Ingrediente.class, new Custom()).create();
    }

    public void loadIngredientes(){
        // Load Promocoes data
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url ="http://192.168.15.11:8080/api/ingrediente";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        gson.fromJson(response, Ingrediente.class);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO show error to the user on screen

                String err = error.toString();
                err=err+"a";
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    class Custom implements JsonDeserializer<ArrayList<Ingrediente>> {
        @Override
        public ArrayList<Ingrediente> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonArray jsonArray = json.getAsJsonArray();

            for (JsonElement jsonElement:jsonArray) {
                Ingrediente ingrediente = new Ingrediente();
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                ingrediente.mId = jsonObject.get("id").getAsNumber();
                ingrediente.mName = jsonObject.get("name").getAsString();
                ingrediente.mPrice = jsonObject.get("price").getAsFloat();

                ingrediente.mImageURL = jsonObject.get("image").getAsString();

                Ingredientes.getInstance().addIngrediente(ingrediente);

            }

            return null;
        }
    }
}
