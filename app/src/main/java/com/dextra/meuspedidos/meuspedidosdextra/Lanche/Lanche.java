package com.dextra.meuspedidos.meuspedidosdextra.Lanche;

import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Felipe on 11/27/18.
 */

public class Lanche {
    public Number mId;
    public String mName;
    public int[] mIngredientes;
    public URL mFoto;

    public Lanche() {
    }

    public Lanche(Lanche lanche){
        mId = lanche.mId;
        mName = lanche.mName;
        mIngredientes = lanche.mIngredientes;
        mFoto = lanche.mFoto;

       // id = valueAt(map, "id");
    }

    public void setName(Number id){
        mId = id;
    }

    public void setName(String name){
        mName = name;
    }

    public void setIngredients(int[] ingredientes){
        mIngredientes = ingredientes;
    }

    public void setName(URL foto){
        mFoto = foto;
    }
}
