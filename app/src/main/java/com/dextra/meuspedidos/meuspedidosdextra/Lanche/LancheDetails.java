package com.dextra.meuspedidos.meuspedidosdextra.Lanche;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dextra.meuspedidos.meuspedidosdextra.Cardapio.Cardapio;
import com.dextra.meuspedidos.meuspedidosdextra.Ingredientes.Ingrediente;
import com.dextra.meuspedidos.meuspedidosdextra.Ingredientes.Ingredientes;
import com.dextra.meuspedidos.meuspedidosdextra.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class LancheDetails extends AppCompatActivity {

    private Lanche lanche;
    private ImageView mFoto;
    private TextView mName, mPreco;
    private ListView mIngredientesListView;
    private Button maisButton, menosButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lanche_details);

        Intent intent = getIntent();
        int LancheId = intent.getIntExtra("LancheId", 0);
        lanche = Cardapio.getInstance().getLanche(LancheId-1);
        getElements();
        setElements();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void getElements(){

        mFoto = findViewById(R.id.LancheImageView);
        mName = findViewById(R.id.lancheNameTextView);
        mIngredientesListView = findViewById(R.id.ingredientesListView);
        maisButton = findViewById(R.id.maisButton);
        menosButton = findViewById(R.id.menosButton);
        mPreco = findViewById(R.id.precoTextView);
    }

    private void setElements(){

        Picasso.get().load(lanche.mFoto.toString()).into(mFoto);
        mName.setText(lanche.mName);
        float preco = 0;
        ArrayList ingredienteslist = new ArrayList();

        //Get ingredients and price
        for (int ingrediente: lanche.mIngredientes) {
            ingredienteslist.add(Ingredientes.getInstance().getIngrediente(ingrediente-1));
            preco += Ingredientes.getInstance().getIngredientePreco(ingrediente-1);

        }
        
        //Set decimal mask
        NumberFormat df = DecimalFormat.getInstance();
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        preco = Float.parseFloat( df.format(preco));

        mPreco.setText("R$"+preco);

        ArrayAdapter adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.lanche_list_layout, ingredienteslist);
        mIngredientesListView.setAdapter(adapter);
    }
}
