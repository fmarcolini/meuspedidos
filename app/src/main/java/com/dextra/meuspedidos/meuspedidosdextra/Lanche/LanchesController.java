package com.dextra.meuspedidos.meuspedidosdextra.Lanche;

import android.content.Context;

import com.dextra.meuspedidos.meuspedidosdextra.Cardapio.Cardapio;

import java.util.ArrayList;

/**
 * Created by Felipe on 12/1/18.
 */

public class LanchesController {

    private LanchesModel lanchesModel;


    public LanchesController(Context context){
        lanchesModel = new LanchesModel(context);
    }

    public void getLanches(){
        lanchesModel.loadLanches();
    }


}
