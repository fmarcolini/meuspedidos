package com.dextra.meuspedidos.meuspedidosdextra.Lanche;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dextra.meuspedidos.meuspedidosdextra.Cardapio.Cardapio;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Felipe on 12/1/18.
 */

public class LanchesModel {

    private Context mContext;

    private Gson gson;
    public LanchesModel(Context Context){
        mContext = Context;
        gson = new GsonBuilder().registerTypeAdapter(Lanche.class, new Custom()).create();
    }


    public void loadLanches() {

        // Load lanche data
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url ="http://192.168.15.11:8080/api/lanche";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //populate Cardápio singleton
                        gson.fromJson(response, Lanche.class);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //TODO show error to the user on screen
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    class Custom implements JsonDeserializer<Lanche> {
        @Override
        public Lanche deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            Lanche lanche = new Lanche();
            JsonArray jsonArray = json.getAsJsonArray();

            for (JsonElement jsonElement:jsonArray) {

                JsonObject jsonObject = jsonElement.getAsJsonObject();
                lanche.mId = jsonObject.get("id").getAsNumber();
                lanche.mName = jsonObject.get("name").getAsString();

                String ingredientsString = jsonObject.get("ingredients").toString();
                lanche.mIngredientes = convertStringArraytoInt(ingredientsString);

                String imageString = jsonObject.get("image").getAsString();

                URL url = null;
                try {
                    url = new URL(imageString);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                lanche.mFoto = url;

                Cardapio.getInstance().addLanche(new Lanche(lanche));

            }

            return lanche;
        }
    }

    private int[] convertStringArraytoInt(String array){
        String[] items = array.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");

        int[] results = new int[items.length];

        for (int i = 0; i < items.length; i++) {
            try {
                results[i] = Integer.parseInt(items[i]);
            } catch (NumberFormatException nfe) {
                //NOTE: write something here if you need to recover from formatting errors
            };
        }
        return results;
    }

}
