package com.dextra.meuspedidos.meuspedidosdextra.Lanche;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dextra.meuspedidos.meuspedidosdextra.Cardapio.Cardapio;
import com.dextra.meuspedidos.meuspedidosdextra.Lanche.LanchesFragment.OnListFragmentInteractionListener;
import com.dextra.meuspedidos.meuspedidosdextra.R;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Cardapio} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyLanchesRecyclerViewAdapter extends RecyclerView.Adapter<MyLanchesRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<Lanche> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyLanchesRecyclerViewAdapter(Cardapio cardapio, OnListFragmentInteractionListener listener) {
        mValues = cardapio.getCardapio();
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_lanches, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).mId.toString());
        holder.mNameView.setText(mValues.get(position).mName);

        Picasso.get().load(mValues.get(position).mFoto.toString()).into(holder.mImageView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mNameView;
        public final ImageView mImageView;
        public Lanche mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mNameView = (TextView) view.findViewById(R.id.name);
            mImageView = (ImageView) view.findViewById(R.id.image);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
