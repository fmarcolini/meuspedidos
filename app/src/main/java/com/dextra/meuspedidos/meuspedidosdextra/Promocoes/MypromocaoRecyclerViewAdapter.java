package com.dextra.meuspedidos.meuspedidosdextra.Promocoes;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dextra.meuspedidos.meuspedidosdextra.Promocoes.promocaoFragment.OnListFragmentInteractionListener;
import com.dextra.meuspedidos.meuspedidosdextra.R;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Promocao} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MypromocaoRecyclerViewAdapter extends RecyclerView.Adapter<MypromocaoRecyclerViewAdapter.ViewHolder> {

    private final List<Promocao> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MypromocaoRecyclerViewAdapter(Promocoes promocoes, OnListFragmentInteractionListener listener) {
        mValues = promocoes.getPromocoes();
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_promocao, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).mId.toString());
        holder.mNameView.setText(mValues.get(position).mName);
        holder.mContentView.setText(mValues.get(position).mDescription);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mNameView;
        public final TextView mContentView;
        public Promocao mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mNameView = (TextView) view.findViewById(R.id.name);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
