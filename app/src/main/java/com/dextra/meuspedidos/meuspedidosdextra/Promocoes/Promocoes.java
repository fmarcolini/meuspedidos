package com.dextra.meuspedidos.meuspedidosdextra.Promocoes;

import java.util.ArrayList;

/**
 * Created by Felipe on 12/2/18.
 */

public class Promocoes {
    private static final Promocoes ourInstance = new Promocoes();

    public static Promocoes getInstance() {
        return ourInstance;
    }

    private ArrayList<Promocao> mPromocoes;

    private Promocoes() {
        mPromocoes = new ArrayList<Promocao>();
    }

    public ArrayList<Promocao> getPromocoes(){
        return mPromocoes;
    }

    public void addPromocao(Promocao promocao){
        mPromocoes.add(promocao);
    }

    public Promocao getPromocao(int id){
        return mPromocoes.get(id);
    }

}
