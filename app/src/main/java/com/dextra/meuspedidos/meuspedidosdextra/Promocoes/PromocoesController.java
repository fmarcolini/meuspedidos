package com.dextra.meuspedidos.meuspedidosdextra.Promocoes;

import android.content.Context;

/**
 * Created by Felipe on 12/2/18.
 */

public class PromocoesController {

    private PromocoesModel promocoesModel;


    public PromocoesController(Context context){
        promocoesModel = new PromocoesModel(context);
    }

    public void getPromocoes(){
        promocoesModel.loadPromocoes();
    }
}
