package com.dextra.meuspedidos.meuspedidosdextra.Promocoes;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dextra.meuspedidos.meuspedidosdextra.Cardapio.Cardapio;
import com.dextra.meuspedidos.meuspedidosdextra.Lanche.Lanche;
import com.dextra.meuspedidos.meuspedidosdextra.Lanche.LanchesModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Felipe on 12/2/18.
 */

public class PromocoesModel {

    private Context mContext;
    private Gson gson;

    public PromocoesModel(Context context){
        mContext = context;
        gson = new GsonBuilder().registerTypeAdapter(Promocao.class, new Custom()).create();
    }

    public void loadPromocoes(){
        // Load Promocoes data
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url ="http://192.168.15.11:8080/api/promocao";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        gson.fromJson(response, Promocao.class);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO show error to the user on screen
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    class Custom implements JsonDeserializer<ArrayList<Promocao>> {
        @Override
        public ArrayList<Promocao> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {



            JsonArray jsonArray = json.getAsJsonArray();

            for (JsonElement jsonElement:jsonArray) {
                Promocao promocao = new Promocao();
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                promocao.mId = jsonObject.get("id").getAsNumber();
                promocao.mName = jsonObject.get("name").getAsString();
                promocao.mDescription = jsonObject.get("description").getAsString();


                Promocoes.getInstance().addPromocao(promocao);

            }

            return null;
        }
    }
}
