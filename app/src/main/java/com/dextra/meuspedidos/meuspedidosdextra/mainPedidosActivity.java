package com.dextra.meuspedidos.meuspedidosdextra;



import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.dextra.meuspedidos.meuspedidosdextra.Cardapio.Cardapio;
import com.dextra.meuspedidos.meuspedidosdextra.Ingredientes.Ingrediente;
import com.dextra.meuspedidos.meuspedidosdextra.Ingredientes.Ingredientes;
import com.dextra.meuspedidos.meuspedidosdextra.Ingredientes.IngredientesController;
import com.dextra.meuspedidos.meuspedidosdextra.Lanche.Lanche;
import com.dextra.meuspedidos.meuspedidosdextra.Lanche.LancheDetails;
import com.dextra.meuspedidos.meuspedidosdextra.Lanche.LanchesController;
import com.dextra.meuspedidos.meuspedidosdextra.Lanche.LanchesFragment;
import com.dextra.meuspedidos.meuspedidosdextra.Promocoes.Promocao;
import com.dextra.meuspedidos.meuspedidosdextra.Promocoes.Promocoes;
import com.dextra.meuspedidos.meuspedidosdextra.Promocoes.PromocoesController;
import com.dextra.meuspedidos.meuspedidosdextra.Promocoes.promocaoFragment;
import com.dextra.meuspedidos.meuspedidosdextra.carrinho.CarrinhoFragment;

public class mainPedidosActivity extends AppCompatActivity
        implements LanchesFragment.OnListFragmentInteractionListener,
                    CarrinhoFragment.OnFragmentInteractionListener,
                    promocaoFragment.OnListFragmentInteractionListener{

    private Fragment lancheFragment, carrinhoFragment, promocoesFragment;
    private LanchesController lanchesControler;
    private PromocoesController promocoesController;
    private IngredientesController ingredientesController;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_burguers:

                    // Create new fragment and transaction
                    lancheFragment = new LanchesFragment();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    transaction.replace(R.id.fragmentContainer, lancheFragment);
                    transaction.addToBackStack(null);

                    // Commit the transaction
                    transaction.commit();

                    return true;

                case R.id.navigation_sale:
//                    // Create new fragment and transaction
                    promocoesFragment = new promocaoFragment();
                    FragmentTransaction transactionSales = getSupportFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    transactionSales.replace(R.id.fragmentContainer, promocoesFragment);
                    transactionSales.addToBackStack(null);

                    // Commit the transaction
                    transactionSales.commit();
                    return true;

                case R.id.navigation_cart:
//                    // Create new fragment and transaction
                    carrinhoFragment = new CarrinhoFragment();
                    FragmentTransaction transactionCart = getSupportFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    transactionCart.replace(R.id.fragmentContainer, carrinhoFragment);
                    transactionCart.addToBackStack(null);

                    // Commit the transaction
                    transactionCart.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onListFragmentInteraction(Lanche item){

        Intent intent = new Intent(mainPedidosActivity.this, LancheDetails.class);
        intent.putExtra("LancheId", Integer.parseInt(""+item.mId));
        startActivity(intent);

    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_pedidos);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        lanchesControler = new LanchesController(getApplicationContext());
        promocoesController = new PromocoesController(getApplicationContext());
        ingredientesController = new IngredientesController(getApplicationContext());

    }

    @Override
    protected void onResume() {
        super.onResume();


        if(Cardapio.getInstance().getCardapio().size() <= 0 ){
            lanchesControler.getLanches();
        }

        if(Promocoes.getInstance().getPromocoes().size() <= 0){
            promocoesController.getPromocoes();
        }

        if(Ingredientes.getInstance().getIngredientes().size() <= 0 ){
            ingredientesController.getIngredientes();
        }


        // Create new fragment and transaction
        lancheFragment = new LanchesFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragmentContainer, lancheFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    private boolean loadFragment(Fragment fragment){

        if(fragment!=null){

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer,fragment)
                    .commit();
            return true;
        }
        return false;
    }


    @Override
    public void onListFragmentInteraction(Promocao item) {

    }
}
